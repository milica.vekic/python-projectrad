import sqlite3
connection=sqlite3.connect("grupe_vrtic.py")
cursor=connection.cursor()
sql_command="""
CREATE TABLE Grupe(
ID_GRUPE INTEGER PRIMARY KEY,
NAZIV_GRUPE VARCHAR(50),
BROJ_POLAZNIKA INTEGER,
)
"""
cursor.execute(sql_command)
connection.close()
from tabulate import tabulate
import sqlite3
connection=sqlite3.connect("grupe_vrtic.py")

def unos_informacija(ID_GRUPE, NAZIV_GRUPE, BROJ_POLAZNIKA):
    qry="INSERT INTO Grupe(ID_GRUPE,NAZIV_GRUPE, BROJ_POLAZNIKA)VALUES(?,?,?);"
    connection.execute(qry,(ID_GRUPE,NAZIV_GRUPE,BROJ_POLAZNIKA))
    connection.commit()
    print("Dodana je nova grupa")
def izmjena_informacija(ID_GRUPE,NAZIV_GRUPE,BROJ_POLAZNIKA):
    qry="UPDATE Grupe set ID_GRUPE=?,NAZIV_GRUPE=?,BROJ_POLAZNIKA=? WHERE ID_GRUPE=?;"
    connection.execute(qry,(ID_GRUPE,NAZIV_GRUPE,BROJ_POLAZNIKA))
    connection.commit()
    print("Podaci o grupi su izmijenjeni")
    res=connection.cursor()
    qry="UPDATE Grupe set ID_GRUPE=?,NAZIV_GRUPE=?,BROJ_POLAZNIKA=? WHERE ID_GRUPE=?;"
    grupe=(ID_GRUPE,NAZIV_GRUPE,BROJ_POLAZNIKA)
    res.execute(qry,grupe)
    connection.commit()
    print("Uspjesno je izvrsna izmjena")

def brisanje_informacija(ID_GRUPE):
    qry="DELETE FROM Grupe WHERE ID_GRUPE=?;"
    connection.execute(qry,(ID_GRUPE))
    connection.commit()
    print("Podaci su obrisani")
    res=connection.cursor()
    qry="DELETE FROM Grupe WHERE ID_GRUPE=?;"
    izbrisi=(ID_GRUPE)
    res.execute(qry,izbrisi)
    connection.commit()
    print("Podaci su obrisani")

def selektovanje_informacija():
    res=connection.cursor()
    qry="SELECT * FROM Grupe"
    res.execute(qry)
    rezultat=res.fetchall()
    print(tabulate(rezultat,headers=["ID_GRUPE","NAZIV_GRUPE","BROJ_POLAZNIKA"]))
    if res.rowcount<0:
        print("Nema podataka")
    else:
        for podaci in rezultat:
            print(podaci)
def prikaz_meni(connection):
    print("Prikazi opcije")
    print("1. Ubacivanje podataka")
    print("2. Selektovanje podataka")
    print("3. Izmjena podataka")
    print("4. Brisanje podataka")
    menu=input("Izaberi opciju:")
    if menu=="1":
        unos_informacija(connection)
    elif menu=="2":
        selektovanje_informacija(connection)
    elif menu=="3":
        izmjena_informacija(connection)
    elif menu=="4":
        brisanje_informacija(connection)
    else:
        print("Greska")
    if __name__=="__main__":
        while(True):
            prikaz_meni(connection)