import sqlite3
from datetime import datetime

from pip._internal.utils.misc import tabulate

connection=sqlite3.connect("djeciji_vrtic.py")
cursor=connection.cursor()
sql_comm="""
CREATE TABLE Polaznici(
ID_POLAZNIKA INTEGER PRIMARY KEY,
IME_POLAZNIKA VARCHAR(50),
PREZIME_POLAZNIKA VARCHAR(50),
DATUM_RODJENJA DATE,
JMBG_POLAZNIKA INTEGER,
POL CHAR,
IME_RODITELJA VARCHAR(30)
KONTAKT_RODITELJA INTEGER,
NAZIV_GRUPE VARCHAR(50);
"""
cursor.execute(sql_comm)
connection.close()
import sqlite3
connection=sqlite3.connect("djeciji_vrtic.py")

def unos_informacija(ID_POLAZNIKA, IME_POLAZNIKA,PREZIME_POLAZANIKA,DATUM_RODJENJA,JMBG_POLAZNIKA,POL,IME_RODITELJA,KONTAKT_RODITELJA,NAZIV_GRUPE):
    DATUM_RODJENJA=datetime.strptime (DATUM_RODJENJA,'%d/%m/%Y').date()
    qry="INSERT INTO Polaznici(ID_POLAZNIKA, IME_POLAZNIKA, PREZIME_POLAZNIKA, DATUM_RODJENJA,JMBG_POLAZNIKA,POL,IME_RODITELJA,KONTAKT_RODITELJA,NAZIV_GRUPE)VALUES(?,?,?,?,?,?,?,?,?);"
    connection.execute(qry,ID_POLAZNIKA, IME_POLAZNIKA,PREZIME_POLAZANIKA, DATUM_RODJENJA,JMBG_POLAZNIKA,POL,IME_RODITELJA,KONTAKT_RODITELJA,NAZIV_GRUPE)
    connection.commit()
    print("Dodali smo novog polaznika")

def izmjena_informacija(ID_POLAZNIKA, IME_POLAZNIKA, PREZIME_POLAZNIKA, DATUM_RODJENJA,JMBG_POLAZNIKA,POL,IME_RODITELJA,KONTAKT_RODITELJA,NAZIV_GRUPE):
    qry="UPDATE Polaznici set IME_POLAZNIKA=?, PREZIME_POLAZNIKA=?, DATUM_RODJENJA=?,JMBG_POLAZNIKA=?,POL=?,IME_RODITELJA=?,KONTAKT_RODJENJA=?,NAZIV_GRUPE=? WHERE ID_POLAZNIKA=?;"
    connection.execute(qry(IME_POLAZNIKA, PREZIME_POLAZNIKA, DATUM_RODJENJA,JMBG_POLAZNIKA,POL,IME_RODITELJA,KONTAKT_RODITELJA,NAZIV_GRUPE,ID_POLAZNIKA))
    connection.commit()
    print("Izmijenili smo podatke o polazniku")
    res=connection.cursor()
    qry="UPDATE Polaznici set IME_POLAZNIKA=?, PREZIME_POLAZNIKA=?, DATUM_RODJENJA=?,JMBG_POLAZNIKA=?,POL=?,IME_RODITELJA=?,KONTAKT_RODJENJA=?,NAZIV_GRUPE=? WHERE ID_POLAZNIKA=?;"
    polaznik=(IME_POLAZNIKA,PREZIME_POLAZNIKA,DATUM_RODJENJA,JMBG_POLAZNIKA,POL,IME_RODITELJA,KONTAKT_RODITELJA,NAZIV_GRUPE,ID_POLAZNIKA)
    res.execute=(qry,polaznik)
    connection.commit()
    print("Izmijenili smo podatke o polazniku uspjesno")

def brisanje_informacija(ID_POLAZNIKA):
    qry="DELETE FROM Polaznici WHERE ID_POLAZNIKA=?;"
    connection.execute(qry,(ID_POLAZNIKA))
    connection.commit()
    print("Uspjesno smo obrisali podatke o polazniku")
    res=connection.cursor()
    qry="DELETE FROM Polaznici WHERE ID_POLAZNIKA=?;"
    izbrisi=(ID_POLAZNIKA)
    res.execute(qry,izbrisi)
    connection.commit()
    print("Podaci su uspjesno obrisani")

def selektovanje_informacija():
    res=connection.cursor()
    qry="SELECT * FROM Polaznici"
    res.execute(qry)
    rezultat=res.fetchall()
    print(tabulate(rezultat,headers=["ID_POLAZNIKA", "IME_POLAZNIKA","PREZIME_POLAZNIKA","DATUM_RODJENJA","JMBG_POLAZNIKA","POL","IME_RODITELJA","KONTAKT_RODITELJA","NAZIV_GRUPE"]))
    if res.rowcount<0:
        print("Nisu pronadjeni podaci")
    else:
        for podaci in rezultat:
            print(podaci)
print("""
1. Unosenje novih podataka.
2. Promjena podataka.
3. Brisanje podataka.
4. Selektovanje podataka.
""")

unos_podataka=1
while unos_podataka==1:
    unos=int(input("Izaberi opciju:"))
    if(unos==1):
        print("Dodajte novog polaznika")
        ID_POLAZNIKA=int(input("Unesi ID:"))
        IME_POLAZNIKA=input("Unesi ime")
        PREZIME_POLAZNIKA=input("Unesi prezime")
        DATUM_RODJENJA=input("Unesi datum rodjenja(format:dd/mm/yyyy):")
        JMBG_POLAZNIKA=int(input("Unesi jmbg"))
        IME_RODITELJA=input("Unesi ime roditelja")
        KONTAKT_RODITELJA=int(input("Unesi kontakt roditelja"))
        POL=input("Unesi pol polaznika")
        NAZIV_GRUPE=input("Unesi naziv grupe polaznika")
        unos_informacija(ID_POLAZNIKA,IME_POLAZNIKA,PREZIME_POLAZNIKA,DATUM_RODJENJA,JMBG_POLAZNIKA, POL, IME_RODITELJA, KONTAKT_RODITELJA, NAZIV_GRUPE)

    if(unos==2):
        print("Promijeni podatke o polazniku")
        ID_POLAZNIKA = int(input("Unesi ID:"))
        IME_POLAZNIKA = input("Unesi ime")
        PREZIME_POLAZNIKA = input("Unesi prezime")
        DATUM_RODJENJA = input("Unesi datum rodjenja(format:dd/mm/yyyy):")
        JMBG_POLAZNIKA = int(input("Unesi jmbg"))
        IME_RODITELJA = input("Unesi ime roditelja")
        KONTAKT_RODITELJA = int(input("Unesi kontakt roditelja"))
        POL = input("Unesi pol polaznika")
        NAZIV_GRUPE = input("Unesi naziv grupe polaznika")
        promjena_informacija=(ID_POLAZNIKA,IME_POLAZNIKA,PREZIME_POLAZNIKA,DATUM_RODJENJA,JMBG_POLAZNIKA,POL,IME_RODITELJA,KONTAKT_RODITELJA,NAZIV_GRUPE)

    if(unos==3):
        print("Brisi podatke o polazniku.")
        ID_POLAZNIKA = int(input("Unesi ID kako bi se obrisali podaci o polazniku:"))
        brisanje_informacija(ID_POLAZNIKA)

    if(unos==4):
        print("Ispisi informacije o polazniku")
        selektovanje_informacija()
    else:
        print("Greska.Probaj opet")



