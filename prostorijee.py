import sqlite3
connection=sqlite3.connect("prostorije.py")
cursor=connection.cursor()
sql_command="""
CREATE TABLE Prostorije(
ID_Prostorije INTEGER PRIMARY KEY, 
NAZIV_PROSTORIJE VARCHAR(50),
KAPACITET INTEGER;
)
"""
cursor.execute(sql_command)
connection.close()
from tabulate import tabulate
import sqlite3
connection=sqlite3.connect("prostorije.py")

def unos_informacija(ID_Prostorije,NAZIV_PROSTORIJE,KAPACITET):
    qry="INSERT INTO Prostorije(ID_Prostorije, NAZIV_PROSTORIJE,KAPACITET)VALUES (?,?,?);"
    connection.execute(qry,(ID_Prostorije,NAZIV_PROSTORIJE,KAPACITET))
    connection.commit()
    print("Dodali smo informaciju o prostoriji")
def izmjena_informacija(ID_Prostorije,NAZIV_PROSTORIJE,KAPACITET):
    qry="UPDATE Prostorije set ID_Prostorije=?,NAZIV_PROSTORIJE=?,KAPACITET=?"
    connection.execute(qry,(ID_Prostorije,NAZIV_PROSTORIJE,KAPACITET))
    connection.commit()
    print("Podaci o prostoiji su izmijenjeni")
    res=connection.cursor()
    qry="UPDATE Prostorije set ID_Prostorije=?, NAZIV_PROSTORIJE=?,KAPACITET=? WHERE ID_Prostorije=?;"
    prostorija=(ID_Prostorije,NAZIV_PROSTORIJE,KAPACITET)
    res.execute(qry,prostorija)
    connection.commit()
    print("Uspjesno je izvrsena izmjena")
def brisanje_informacija(ID_Prostorija):
    qry="DELETE FROM Prostorije WHERE ID_Prostorije=?;"
    connection.execute(qry,(ID_Prostorija))
    connection.commit()
    print("Podaci su obrisani")
    res=connection.cursor()
    qry="DELETE FROM Prostorije WHERE ID_Prostorije=?"
    izbrisi=(ID_Prostorija)
    res.execute(qry,izbrisi)
    connection.commit()
    print("Podaci su obrisani")

def selektovanje_informacija():
    res=connection.cursor()
    qry="SELECT * FROM Prostorije"
    res.execute(qry)
    rezultat=res.fetchall()
    print(tabulate(rezultat,headers=["ID_Prostorije","NAZIV_PROSTORIJE","KAPACITET"]))
    if res.rowcount < 0:
        print("Nema podataka")
    else:
        for podaci in rezultat:
            print(podaci)


def prikaz_meni(connection):
    print("Prikazi opcije")
    print("1. Ubacivanje podataka")
    print("2. Selektovanje podataka")
    print("3. Izmjena podataka")
    print("4. Brisanje podataka")
    menu = input("Izaberi opciju:")
    if menu == "1":
        unos_informacija(connection)
    elif menu == "2":
        selektovanje_informacija(connection)
    elif menu == "3":
        izmjena_informacija(connection)
    elif menu == "4":
        brisanje_informacija(connection)
    else:
        print("Greska")
    if __name__ == "__main__":
        while (True):
            prikaz_meni(connection)










